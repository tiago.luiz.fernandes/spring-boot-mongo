package br.com.tts.springbootmongo.service;

import br.com.tts.springbootmongo.entity.Order;
import br.com.tts.springbootmongo.repository.mongo.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository pedidoRepository;

    public void create(Order pedido) {
        pedidoRepository.save(pedido);
    }

    public Optional<Order> findById(String id) {
        return pedidoRepository.findById(id);
    }

    public List<Order> findAll() {
        return pedidoRepository.findAll();
    }

}
