package br.com.tts.springbootmongo.config;

import br.com.tts.springbootmongo.entity.Order;
import br.com.tts.springbootmongo.entity.OrderItem;
import br.com.tts.springbootmongo.enumeration.TipoItem;
import br.com.tts.springbootmongo.repository.mongo.OrderRepository;
import br.com.tts.springbootmongo.service.OrderService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class Start {

    @Bean
    CommandLineRunner commandLineRunner(
            OrderRepository orderRepository,
            OrderService orderService){
        return args -> {

            Order order1 = new Order();
            order1.setId("CO000001");

            OrderItem orderItem1 = new OrderItem();
            orderItem1.setId(1L);
            orderItem1.setTipoItem(TipoItem.AIR);

            OrderItem orderItem2 = new OrderItem();
            orderItem2.setId(2L);
            orderItem2.setTipoItem(TipoItem.HOT);

            OrderItem orderItem3 = new OrderItem();
            orderItem3.setId(3L);
            orderItem3.setTipoItem(TipoItem.CAR);

            order1.setItens(List.of(orderItem1,orderItem2,orderItem3));

            Order order2 = new Order();
            order2.setId("CO000002");

            orderItem1 = new OrderItem();
            orderItem1.setId(1L);
            orderItem1.setTipoItem(TipoItem.CAR);

            order2.setItens(List.of(orderItem1));

            orderRepository.saveAll(List.of(order1,order2));

        };
    }

}

