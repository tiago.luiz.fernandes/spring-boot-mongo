package br.com.tts.springbootmongo.repository.mongo;

import br.com.tts.springbootmongo.entity.Order;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends MongoRepository<Order,String> {
}
