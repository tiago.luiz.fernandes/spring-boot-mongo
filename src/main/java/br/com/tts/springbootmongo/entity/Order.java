package br.com.tts.springbootmongo.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter
@Setter
@Document
@ToString
public class Order {

    @Id
    private String id;

    private List<OrderItem> itens;
}
