package br.com.tts.springbootmongo.entity;

import br.com.tts.springbootmongo.enumeration.TipoItem;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;

@Getter
@Setter
@ToString
public class OrderItem {

    @Id
    private Long id;

    private TipoItem tipoItem;
}
