package br.com.tts.springbootmongo.controller;

import br.com.tts.springbootmongo.entity.Order;
import br.com.tts.springbootmongo.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/order")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService pedidoService;

    @GetMapping()
    public List<Order> findAll() {
        return pedidoService.findAll();
    }

    @GetMapping(path = "{id}")
    public Order findById(@PathVariable("id") String id) {
        Optional<Order> optionalPedido = pedidoService.findById(id);
        return optionalPedido.get();
    }

}
